#!/usr/bin/env perl 
use strict; 
use warnings; 
use MIME::Parser; 
use File::Path; 
use Net::IMAP::Simple::SSL; 

#infos compte messagerie 
my $server = "imap.gmail.com"; 
my $user="user\@gmail.com"; 
my $pass="password";
my @date=date(); 
my $dossier_output="mailsPJ_@date"; 

# Suppression du dossier s'il existe déja 
if (-d "$dossier_output") { 
	rmtree(["$dossier_output"], 1, 1); 
} 
# on crée dossier de backup nommé avec la date du jour
mkdir ($dossier_output) or die "impossible de créer le dossier \n";

# authentification auprés du serveur 
my $imap = Net::IMAP::Simple::SSL->new($server); 
$imap->login($user => $pass) or die 'Erreur d\'identification !'; 

# récupération du nombre de messages stockés sur le serveur
my $total_messages = $imap->select("INBOX"); 
print "Nombre de messages : $total_messages\n"; 
print "Appuyer sur la touche Entrée pour lancer le téléchargement...";
<>;
# telecharge toutes les mails et pieces jointes 
foreach my $msg ( 1..$total_messages ) { 
	print "Téléchargement du message n° : $msg\n"; 
	# recup header du mail 
	my $header = $imap->top($msg); 
	my $head_obj = Mail::Header->new($header); 
	#adresse mail de l'expediteur
	my $from = $head_obj->get('From'); 
	my $exp=""; 
	#extraction du nom expediteur 
	if( $from =~ m/^(.*)\<(.*)\@(.*)>/) { 
		$exp=$2.$3; 
		print "$exp\n"; 
	} 
	else{ 
		$exp="Inconnu"; 
		print "$exp\n"; 
	} 
	my $lines = $imap->get( $msg ); 
	#$lines contient header+body+PJ 
	my $fh = $imap->getfh( $msg );
	my ($parser) = new MIME::Parser; 
	# par défaut, un dossier est créé avec un nom peu explicite pour un humain (timestamp current time + processID + number sequence) 
	#$parser->output_under($dossier_output); 
	# Création des dossiers avec nom de l'expediteur
	$parser->output_under($dossier_output, DirName=>"$exp"); 
	# toutes les PJ dans un même dossier
	# $parser->output_dir($dossier_output);
	my $entity = $parser->parse($fh); 
	close $fh; 
}

sub date{
	my($day, $month, $year) = (localtime)[3,4,5]; 
	$month = sprintf '%02d', $month+1; 
	$day = sprintf '%02d', 
	$day; #print $year+1900, $month, $day, "\n"; 
	return (($year+1900).$month.$day); 
}
